﻿using NLog;
using System;
using System.Configuration;
using System.ServiceProcess;
using System.Timers;
using UtilityServices.Business;
using UtilityServices.Business.Plugins;

namespace UtilityServices
{
    public partial class UtilitiesService : ServiceBase
    {
        public UtilitiesService()
        {
            InitializeComponent();
        }
        #region Properties
        // public TimeSpan MainTiming { get; set; }
        // public Timer MainTimer { get; set; }
        public MEFContainer MEFContainer { get; set; }
        public static Logger Logger { get; set; }
        #endregion
        #region Methods

        public void RunAsConsole(string[] args)
        {
            OnStart(args);
           //Console.WriteLine("Press any key to exit...");
            //Console.ReadLine();
            OnStop();
        }

        private static void Main(string[] args)
        {
            UtilitiesService service = new UtilitiesService();

            if (Environment.UserInteractive)
            {
                service.RunAsConsole(args);
                //Console.WriteLine("Press any key to stop program");
                //Console.Read();
                service.OnStop();
            }
            else
            {
                ServiceBase.Run(service);
            }

        }

        protected override void OnStart(string[] args)
        {
            if(args == null || args.Length <= 0)
            {
                Console.WriteLine("Usage UtilitiesService -[d][c][f]");
                Console.WriteLine("hit a key to continue");
                Console.ReadKey();
                return;
            }
            try
            {
                Logger = NLog.LogManager.GetCurrentClassLogger();
                //string mainTiming = ConfigurationManager.AppSettings["UtilityMainTiming"];
                //if (string.IsNullOrEmpty(mainTiming))
                //{
                //    mainTiming = "00:00:01:00";
                //}
                //// this.MainTiming = TimeSpan.Parse(mainTiming);

                //// Create our polling timer
                //// Create a timer with a ten second interval.
                //// this.MainTimer = new System.Timers.Timer(MainTiming.TotalMilliseconds);
                //Logger.Info("Calling all services on timer of {0}", mainTiming);
                // Hook up the Elapsed event for the timer.
                // this.MainTimer.Interval = MainTiming.TotalMilliseconds;
                // this.MainTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                
                // this.MainTimer.Enabled = true;
                // this.MainTimer.AutoReset = true;
                // this.MainTimer.Start();


                // Hook up the Elapsed event for the timer.
                // TODO Get Connection String from file
                 var connectionStr = ConfigurationManager.ConnectionStrings["chimp"].ConnectionString;
                this.MEFContainer = new MEFContainer();
                this.MEFContainer.DoImport();
                foreach (Lazy<IPlugin> plugin in this.MEFContainer.Plugins)
                {                    
                    plugin.Value.Initialize(connectionStr, ConfigurationManager.AppSettings, Logger);
                }

                ProcessPlugins(args[0]);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Logger.Log(LogLevel.Error, ex);
                // ShutdownTimer();
            }
        }

        //private void ShutdownTimer()
        //{
        //    if (this.MainTimer != null)
        //    {
        //        this.MainTimer.Enabled = false;
        //        this.MainTimer.AutoReset = false;
        //        this.MainTimer.Stop();
        //        this.MainTimer.Dispose();
        //    }
        //}

        protected override void OnStop()
        {
            if(this.MEFContainer == null)
            {
                return;
            }
            // ShutdownTimer();
            NLog.LogManager.Shutdown(); // Flush 

            foreach (Lazy<IPlugin> plugin in this.MEFContainer.Plugins)
            {
                plugin.Value.Shutdown();
            }

            #endregion
        }

        private void ProcessPlugins(string commandLineArg)
        {
            commandLineArg = commandLineArg.Replace("-", "");
            try
            {
                foreach (Lazy<IPlugin> plugin in this.MEFContainer.Plugins)
                {
                    if (plugin.Value.Enabled && !plugin.Value.IsProcessing && commandLineArg.ToLower() == plugin.Value.CommandLineArg.ToLower())
                    {
                        plugin.Value.Process();
                    }
                }
            }
            catch(Exception ex)
            {
                string msg = string.Format("Error an exception occurred, message = {0}", ex.Message);
                Console.WriteLine(msg);
                Logger.Log(LogLevel.Error, msg);
            }
        }
    }
}
