﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityServices.Business.Plugins
{
    public interface IPlugin
    {
        void Initialize(string connectionString, NameValueCollection configuration, Logger logger);
        void Process();
        void Shutdown();
        bool IsProcessing { get; set; }
        bool Enabled { get; set; }

        string Name { get; set; }
        NLog.Logger Logger { get; set; }
    }
}
