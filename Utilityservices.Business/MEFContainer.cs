﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using UtilityServices.Business.Plugins;

namespace UtilityServices.Business
{
    public class MEFContainer
    {
        private CompositionContainer compositionContainer = null;

        [ImportMany(typeof(IPlugin))]
        public IEnumerable<Lazy<IPlugin>> Plugins;
        public CompositionContainer Container { get; set; }
        public void DoImport()
        {

            if (compositionContainer == null)
            {
                var catalog = new AggregateCatalog();

                //Add all the parts found in all assemblies in
                //the same directory as the executing program
                catalog.Catalogs.Add(
                    new DirectoryCatalog(
                        Path.GetDirectoryName(
                        Assembly.GetExecutingAssembly().Location
                        )
                    )
                );
                //Create the CompositionContainer with the parts in the catalog.
                CompositionContainer compositionContainer = new CompositionContainer(catalog);

                //Fill the imports of this object
                compositionContainer.ComposeParts(this);
                Container = compositionContainer;

            }

        }
    }

    }

