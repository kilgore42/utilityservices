﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityServices.Business.Plugins;
using UtilityServices.Repository;

namespace Utilityservices.Business.Plugins
{
    [Export(typeof(IPlugin))]
    public class DbCommands: IPlugin
    {
        public bool IsProcessing { get; set; }
        private Repo Repo { get; set; }
        public string Name { get; set; }
        public NLog.Logger Logger { get; set; }
        public bool Enabled { get; set; }
        public String CommandLineArg { get; set; }
        public void Initialize(string connectionString, NameValueCollection configuration, Logger logger)
        {
            IsProcessing = false;
            CommandLineArg = "c";
            this.Repo = new Repo(connectionString);

            string enabled = configuration["DbCommandsEnabled"];
            if (!string.IsNullOrEmpty(enabled))
            {
                this.Enabled = Convert.ToBoolean(enabled);
            }


            this.Logger = logger;
            this.Name = "DbCommands";
            string msg = string.Format("Started {0} Time = {1}", this.Name, DateTime.UtcNow);
            Logger.Info(msg);
        }

        public void Shutdown()
        {
            this.Repo = null;
        }

        public void Process()
        {
            try
            {
                IsProcessing = true;
                string msg = string.Format("{0} StartTime = {1}", this.Name, DateTime.UtcNow);
                this.Logger.Debug(msg);
                Console.WriteLine(msg);
                if (this.Enabled)
                {
                    this.Repo.UpdateOcrQueue();                      
                    msg = string.Format("{0} EndTime = {1} ", this.Name, DateTime.UtcNow);
                    Console.WriteLine(msg);
                    Logger.Info(msg);
                }
            }
            finally
            {
                IsProcessing = false;
            }

        }



    }
}
