﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilityservices.Business;
using UtilityServices.Models;
using UtilityServices.Repository;

namespace UtilityServices.Business.Plugins
{
    [Export(typeof(IPlugin))]
    public class FolderCleanup : IPlugin
    {
        public bool IsProcessing { get; set; }
        private Repo Repo { get; set; }
        public string Name { get; set; }
        public NLog.Logger Logger { get; set; }
        public bool Enabled { get; set; }
        public string InputShare { get; set; }
        public string OutputShare { get; private set; }
        public string TempFolder1 { get; private set; }
        public string TempFolder2 { get; private set; }
        public String CommandLineArg { get; set; }

        public FolderCleanup()
        {

        }
        public void Initialize(string connectionString, NameValueCollection configuration, Logger logger)
        {
            IsProcessing = false;
            this.CommandLineArg = "f";
            this.Repo = new Repo(connectionString);

            string enabled = configuration["FolderCleanupEnabled"];
            if (!string.IsNullOrEmpty(enabled))
            {
                this.Enabled = Convert.ToBoolean(enabled);
            }

            this.InputShare = configuration["InputFolder"] ?? string.Empty;
            this.OutputShare = configuration["OutputFolder"] ?? string.Empty;
            this.TempFolder1 = configuration["TempFolder1"] ?? string.Empty;
            this.TempFolder2 = configuration["TempFolder2"] ?? string.Empty;

            this.Logger = logger;
            this.Name = "FolderCleanup";
            string msg = string.Format("Started {0} Time = {1}, Input Share = {2}, Output Share = {3}", this.Name, DateTime.UtcNow, this.InputShare, this.OutputShare);
            Logger.Info(msg);
        }

        public void Process()
        {
            try
            {
                IsProcessing = true;
                string msg = string.Format("{0} StartTime = {1}", this.Name, DateTime.UtcNow);
                this.Logger.Debug(msg);
                Console.WriteLine(msg);
                if (this.Enabled)
                {
                    long previousStudyId = 0;
                    bool continueReading = true;
                    long maxId = 0;
                    List<FolderInfo> folders = new List<FolderInfo>();
                    Dictionary<long, bool> finishedStudyIds = new Dictionary<long, bool>();
                    //while (continueReading)
                    //{
                    //    if (folders.Any())
                    //    {
                    //        maxId = folders.Last().ImageId;
                    //    }
                    //    Dictionary<string, bool> outputPathsProcessed = new Dictionary<string, bool>();

                    //    folders = this.Repo.GetFoldersToDelete(2500, maxId);
                    //    continueReading = folders != null && folders.Any();
                    //    foreach (var folder in folders)
                    //    {
                    //        if (folder.StudyId != previousStudyId)
                    //        {
                    //            if (previousStudyId > 0)
                    //            {
                    //                if (!finishedStudyIds.ContainsKey(previousStudyId))
                    //                {
                    //                    Repo.UpdateStudyFinished(previousStudyId);
                    //                    finishedStudyIds.Add(previousStudyId, true);
                    //                }
                    //            }
                    //            previousStudyId = folder.StudyId;
                    //        }
                    //        // Generate Full Path to shared folders
                    //        folder.InputPath = string.Format("{0}\\{1}_done", this.InputShare, folder.InputPath);

                    //        if (!string.IsNullOrWhiteSpace(folder.OutputPath))
                    //        {
                    //            folder.OutputPath = string.Format("{0}\\{1}", this.OutputShare, folder.OutputPath);
                    //            if (!outputPathsProcessed.ContainsKey(folder.OutputPath))
                    //            {
                    //                DirectoryInfo diOutput = new DirectoryInfo(folder.OutputPath);
                    //                msg = string.Format("{0} Processing Folder Output Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.OutputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                    //                Logger.Info(msg);
                    //                Console.WriteLine(msg);
                    //                if (diOutput.Exists)
                    //                    TraverseDirectory(diOutput);
                    //                outputPathsProcessed.Add(folder.OutputPath, true);
                    //            }
                    //        }

                    //        DirectoryInfo diInput = new DirectoryInfo(folder.InputPath);
                    //        msg = string.Format("{0} Processing Folder Input Path = {1}, InstId = {2} With Threshold of {3} Days, Age = {4}, ImageId = {5}", this.Name, folder.InputPath, folder.InstId, folder.Threshold, folder.Age, folder.ImageId);
                    //        Logger.Info(msg);
                    //        Console.WriteLine(msg);
                    //        if (diInput.Exists)
                    //            TraverseDirectory(diInput);



                    //        //else
                    //        //{
                    //        //    if (!finishedStudyIds.ContainsKey(folder.StudyId))
                    //        //    {
                    //        //        Repo.UpdateStudyFinished(folder.StudyId);
                    //        //        finishedStudyIds.Add(folder.StudyId, true);
                    //        //    }

                    //        //}
                    //    }

                    //}

                    //// Cover the case of the last one or we never had more than one study id
                    //if (previousStudyId > 0 && !finishedStudyIds.ContainsKey(previousStudyId))
                    //{
                    //    Repo.UpdateStudyFinished(previousStudyId);
                    //    finishedStudyIds.Add(previousStudyId, true);
                    //}

                     ProcessTempFolder("TempFolder1", this.TempFolder1);
                     ProcessTempFolder("TempFolder2", this.TempFolder2);
                    msg = string.Format("{0} EndTime = {1} ", this.Name, DateTime.UtcNow);
                    Console.WriteLine(msg);
                    Logger.Info(msg);
                }
            }
            finally
            {
                IsProcessing = false;
                this.Repo = null;
            }
        }

        private void ProcessTempFolder(string tempName, string tempPath)
        {
            string msg;
            // Process Temp Configured folders and delete their contents:
            if (!string.IsNullOrEmpty(tempPath))
            {
                DirectoryInfo diTemp = new DirectoryInfo(tempPath);
                if (diTemp.Exists)
                {
                    msg = string.Format("{0} Temp Folder {1} Exists = {2} ", this.Name, tempName, tempPath);
                    Console.WriteLine(msg);
                    Logger.Info(msg);
                    msg = string.Format("{0} Processing Temp Folder = {0} ", this.Name, tempPath);
                    Console.WriteLine(msg);
                    Logger.Info(msg);
                    TraverseParentDirectory(diTemp);
                }
                else
                {
                    msg = string.Format("{0} Temp Folder {1} DOES NOT EXIST = {2} ", this.Name, tempName, tempPath);
                    Console.WriteLine(msg);
                    Logger.Error(msg);
                }

            }
        }
        private void TraverseParentDirectory(DirectoryInfo di)
        {
            di.Refresh();
            foreach (var subDir in di.GetDirectories())
            {
                DateTime dt = Directory.GetCreationTime(subDir.FullName.ToString());

                if (DateTime.Today.Subtract(dt).TotalDays > 3)
                    TraverseDirectory(subDir);
            }
        }
        private void TraverseDirectory(DirectoryInfo di)
        {
            di.Refresh();
            foreach (var subDir in di.GetDirectories())
            {
                TraverseDirectory(subDir);
            }

            CleanAllFilesInDirectory(di);
            string msg = string.Format("{0} Deleting Folder {1}", this.Name, di.FullName);
            Console.WriteLine(msg);
            Logger.Info(msg);
            di.Delete();
        }

        private void CleanAllFilesInDirectory(DirectoryInfo di)
        {
            foreach(var file in di.GetFiles())
            {
                file.IsReadOnly = false;
                file.Delete();
                string msg = string.Format("{0} Deleted File {1}", this.Name, file.FullName);
                Console.WriteLine(msg);
                Logger.Info(msg);
            }
        }

        public void Shutdown()
        {
            this.Repo = null;
        }
    }
}
