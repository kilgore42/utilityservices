﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilityservices.Business;
using UtilityServices.Models;
using UtilityServices.Repository;

namespace UtilityServices.Business.Plugins
{
    [Export(typeof(IPlugin))]
     public class DataPurge : IPlugin
    {
        public bool IsProcessing { get; set; }
        // public TimeSpan Frequency { get; set; }
        // public DateTime NextPurge { get; set; }
        private Repo Repo { get; set; }
        public string Name { get; set; }
        public NLog.Logger Logger { get; set; }
        public bool Enabled { get; set; }
        public String CommandLineArg { get; set; }
        public DataPurge()
        {
            this.CommandLineArg = "D";
        }
        public void Initialize(string connectionString, NameValueCollection configuration, Logger logger)
        {
            IsProcessing = false;
            this.Repo = new Repo(connectionString);
            string purgeFrequency = configuration["DataPurgeFrequency"];
            if(string.IsNullOrEmpty(purgeFrequency))
            {
                purgeFrequency = "1:00:00:00";
            }
            if (string.IsNullOrEmpty(purgeFrequency))
            {
                purgeFrequency = "1:00:00:00";
            }

            // this.Frequency = TimeSpan.Parse(purgeFrequency);
            // NextPurge = DateTime.UtcNow;

            string enabled = configuration["DataPurgeEnabled"];
            if(!string.IsNullOrEmpty(enabled))
            {
                this.Enabled = Convert.ToBoolean(enabled);
            }
            
            
            this.Logger = logger;
            this.Name = "DataPurge";
            string msg = string.Format("Started {0} Time = {1}", this.Name, DateTime.UtcNow);
            Logger.Info(msg);

        }

        public void Process()
        {
            try
            {
                IsProcessing = true;
                string msg = string.Format("{0} StartTime = {1}", this.Name, DateTime.UtcNow);
                this.Logger.Debug(msg);
                Console.WriteLine(msg);
                if (true)
                {
                    var config = this.Repo.GetPurgeConfiguration();
                    if (config != null)
                    {
                        foreach (var table in config)
                        {
                            bool foundItemsToDeleteForTable = false;
                            msg = string.Format("{0} Processing Table {1} With Threshold of {2} Days, Id = {3}", this.Name,  table.Name, table.Threshold, table.Id);
                            Logger.Debug(msg);
                            Console.WriteLine(msg);
                            Logger.Info(msg);
                            bool continueProcessing = true;
                            while(continueProcessing)
                            {
                                    List<RowItem> rowsToDelete = Repo.GetdBItemsToDelete(table.Name, table.Threshold, 2500);
                                    continueProcessing = false;
                                    if (rowsToDelete.Any())
                                    {
                                        continueProcessing = true;
                                        foundItemsToDeleteForTable = true;
                                        List<long> ids = rowsToDelete.Select(x => x.Id).ToList();
                                        msg = string.Format(" {0} Table {1} Found {2} Rows over {3} days old",this.Name,  table.Name, ids.Count, table.Threshold);
                                        Console.WriteLine(msg);
                                        Logger.Info(msg);
                                    msg = string.Format("{0} Deleting Items", this.Name);
                                        Console.WriteLine(msg);
                                        Logger.Info(msg);
                                        this.Repo.DeleteItems(table.Name, ids);
                                    }
                            }

                            if(foundItemsToDeleteForTable)
                            {
                                msg = string.Format("{0} Optimizing Table StartTime = {1}", this.Name, DateTime.UtcNow);
                                Console.WriteLine(msg);
                                Logger.Info(msg);
                                this.Repo.OptimizeTable(table.Name);
                                msg = string.Format("{0} Optimizing Table EndTime = {1}", this.Name, DateTime.UtcNow);
                                Console.WriteLine(msg);

                            }

                        }
                        // NextPurge = DateTime.UtcNow + this.Frequency;
                        //Console.WriteLine(msg);

                        msg = string.Format("{0} EndTime = {1} ", this.Name, DateTime.UtcNow);
                        Console.WriteLine(msg);
                        Logger.Info(msg);


                    }
                }
            }
          finally
            {
                IsProcessing = false;
            }
          
        }

        public void Shutdown()
        {
            this.Repo = null;
        }
    }
}
