﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityServices.Models
{
    public class RowItem
    {
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
