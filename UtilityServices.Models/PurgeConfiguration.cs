﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityServices.Models
{
    public class PurgeConfiguration
    {

        //1. Daily task to remove old temporary table records from.
        //  a.moviefiles - where datecreated > xx days
        //  b.ocrfiles - where datecreated > xx days
        //  c.ocrqueue - where datecreated > xx days
        //  d.processaudit -  where datecreated > xx days
        //  e.compact the database after deletes
        public int Id { get; set; }
        public string Name { get; set; }
        public int Threshold { get; set; }
    }
}
